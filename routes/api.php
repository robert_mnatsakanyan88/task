<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);
Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function(){
    Route::get('user', [\App\Http\Controllers\AuthController::class, 'user']);
    Route::post('logout', [\App\Http\Controllers\AuthController::class, 'logout']);
    Route::post('loyalty_discount', [\App\Http\Controllers\AuthController::class, 'add_loyalty_discount_to_user']);
    Route::post('add_product', [\App\Http\Controllers\ProductsController::class, 'add_product']);
    Route::get('products', [\App\Http\Controllers\ProductsController::class, 'all_products']);
    Route::get('product_by_id/{id}', [\App\Http\Controllers\ProductsController::class, 'product_by_id']);
    Route::post('add_daily_discount', [\App\Http\Controllers\ProductsController::class, 'add_daily_discount_for_product']);
    Route::post('make_order', [\App\Http\Controllers\OrdersController::class, 'make_order']);
});