<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Inventory;
use App\Models\Daily_discounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    /**
     * This function adds new product to currently logged in employee
     *
     * @param $name , $description, $unit_price, $employee_id 
     * 
     */
    public function add_product(Request $request){
        if(Auth::user()->type === 'customer'){
            return response([
                'message' => 'Access denied, customers can not create a product'
            ], 403);
        }
        try{
            $product = Products::create([
                'name' => $request->input('name'),
                'description'=> $request->input('description'),
                'unit_price' => $request->input('unit_price'),
                'employee_id'=> Auth::user()->id
            ]);

            Inventory::create([
                'product_id' => $product->id,
                'employee_id' =>  Auth::user()->id,
                'available_weight' => $request->input('weight'),
                'inserted_weight' => $request->input('weight'),
            ]);

            return response([
                'message' =>  'Product was successfully added'
            ], 200);
            }
        catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response([
                'message' =>  $errorInfo
            ], 500);
        }
    }

    /**
     * This function adds discount precentage to a selected product
     *
     * @param $product_id , $discount_precentage
     * 
     */

    public function add_daily_discount_for_product(Request $request){
        if(Auth::user()->type === 'customer'){
            return response([
                'message' => 'Access denied, customers can not add daily discount to  a product'
            ], 403);
        }

        try{
            Daily_discounts::create([
                'product_id' => $request->input('product_id'),
                'discount_precentage'=> $request->input('discount_precentage'),
            ]);
            return response([
                'message' =>  'Discount was creaated for that product successfully'
            ], 200);
            }
        catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response([
                'message' =>  $errorInfo
            ], 500);
        }
    }

     /**
     * This function returns all products in store
     * @return array
     */
    public function all_products(){        
        return Products::with('daily_discount')->get();
    }
    /**
     * This function returns product baased on id
     * @params $id 
     * @return array
     */
    public function product_by_id($id){
        return Products::with('daily_discount')->where('id', $id)->get();
    }
}
