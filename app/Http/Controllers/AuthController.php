<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Loyalty_discounts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * This function register a new user
     *
     * @param $name , $email, $type, $password from $request  
     * @return $user
     */

    public function register(Request $request){
        try{
        User::create([
            'name' => $request->input('name'),
            'email'=> $request->input('email'),
            'type' => $request->input('type'),
            'password'=> Hash::make($request->input('password'))
        ]);
        return response([
            'message' =>  'Succesfully registered'
        ], 200);
        }
         catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response([
                'message' =>  $errorInfo
            ], 500);
        }
    }

    /**
     * This function logs in a registered user with email
     *
     * @param  $email from $request  
     * @return $token
    */
    public function login(Request $request){
        if(!Auth::attempt($request->only('email','password'))){
            return response([
                'message' => 'Invalid credentials'
            ], 401);
        }
        $user = Auth::user();
        $token = $user->createToken('token')->plainTextToken;
        $cookie = cookie('jwt' , $token, 60*24); // 1 day
        return response([
            'message' => 'Success',
            'token'    => $token
        ])->withCookie($cookie);
    }

    /**
     * 
     * This function logs out a user by removing the token
     *
    */
    public function logout(){
        $cookie = Cookie::forget('jwt');

        return response([
            'message' => 'Success',          
        ])->withCookie($cookie);
    }

    /**
     * 
     * This function returns user information
     *
    */
    public function user()
    {   
        return  $user = User::with(['loyalty_discount', 'inventory'])->find(Auth::id());
    }


    public function add_loyalty_discount_to_user(Request $request){
        try{
            Loyalty_discounts::create([
                'user_id' => $request->input('user_id'),
                'discount_precentage'=> $request->input('discount_precentage'),
            ]);
            return response([
                'message' =>  'Discount was creaated for that user successfully'
            ], 200);
            }
        catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response([
                'message' =>  $errorInfo
            ], 500);
        }
    }
}
