<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Inventory;
use App\Models\Daily_discounts;
use App\Models\Orders;
use App\Models\User;
use App\Models\Digital_receipts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class OrdersController extends Controller
{
    public function make_order(Request $request)
    {
        try{
            $discount = 0;
            $loyalty_discount = false;
            $user = User::with(['loyalty_discount'])->find(Auth::id());
            if($user->loyalty_discount){
                $discount = $user->loyalty_discount->discount_precentage;
                $loyalty_discount = true;
            }


            $product = Products::where('id', $request->input('product_id'))->with('daily_discount')->first();
            if(!$product){
                return response([
                    'message' =>  'Product not found'
                ], 200);
            }
            $daily_discount = false;
            if($product->daily_discount){
                $discount = $discount + $product->daily_discount->discount_precentage;
                $daily_discount = true;
            }
            $total_amount_without_discount = $product->unit_price*$request->input('product_quantity');
            $total_amount_with_discount = $total_amount_without_discount - $total_amount_without_discount*$discount/100;
            $discount_type = '';
            if($loyalty_discount && !$daily_discount){
                $discount_type = 'loyalty';
            }else if(!$loyalty_discount && $daily_discount){
                $discount_type = 'daily';
            }else if($loyalty_discount && $daily_discount){
                $discount_type = 'united';
            }else{
                $discount_type = 'no_discount';
            }
            $inventory_item = Inventory::where('product_id',  $product->id)->first();
            if($request->input('product_quantity') > $inventory_item->available_weight){
                return response([
                    'message' =>  'Not enough product for your order'
                ], 200);
            }
            $order = Orders::create([
                'user_id' => Auth::user()->id,
                'product_id'=> $product->id,
                'product_quantity' => $request->input('product_quantity'),
                'total_amount' => $total_amount_with_discount,
                'discount_type' => $discount_type
            ]);
            
            $inventory_item->update([
                'available_weight' => $inventory_item->available_weight - $request->input('product_quantity'),              
            ]);
            Digital_receipts::create([
                'order_id' => $order->id
            ]);
            return response([
                'message' =>  'Order was sucessfully placed',
                'Digital Receipt' => [
                    'order _id' => $order->id,
                    'product_name' => $product->name,
                    'product_description' => $product->description,
                    'ordered_quantity' =>  $request->input('product_quantity'),
                    'ordered_date' => $order->created_at
                ]
            ], 200);
            }
        catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            return response([
                'message' =>  $errorInfo
            ], 500);
        }
    }
}
