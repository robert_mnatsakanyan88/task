<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Digital_receipts extends Model
{
    use HasFactory;

    protected $table =  'digital_receipt';

    protected $fillable = [
        'order_id'
    ];
}
