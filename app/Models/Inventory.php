<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    protected $table =  'inventory';

    protected $fillable = [
        'product_id',
        'employee_id',
        'available_weight',
        'inserted_weight'
    ];

    public function user_inventory(){
        return $this->belongsTo('App\Models\User', 'employee_id', 'id');
    }
}
