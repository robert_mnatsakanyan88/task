<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loyalty_discounts extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'user_id',
        'discount_precentage',
    ];

    public function product(){
        return $this->belongsTo('App\Models\User');
    }
}
