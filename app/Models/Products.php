<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'unit_price',
        'employee_id'
    ];

    public function daily_discount(){
        return $this->hasOne('App\Models\Daily_discounts', 'product_id', 'id');
    }
}
